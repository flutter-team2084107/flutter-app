

import 'package:flutter/material.dart';
import 'package:flutter_app/gridview.dart';
import 'package:flutter_app/home_screen.dart';
import 'package:flutter_app/list_view_page.dart';

import 'package:flutter_app/signup_screen.dart';

import 'login_screen.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: MyLogin(),
    routes: {
      'register': (context) => MyRegister(),
      'login': (context) => MyLogin(),
      'home': (context) => MyHome(),
      'hobbies' : (context) => MyList(),
      'Listview' : (context) => GridExample(),


    },
  ));
}



