import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

void main() => runApp(const GridExample());

class GridExample extends StatelessWidget {
  const GridExample({Key? key}) : super(key: key);

  static const String _title = '';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: _title,
      home: Scaffold(
        appBar: AppBar(title: const Text("Grid view")),
        body: const MyStatefulWidget(),
      ),
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key? key}) : super(key: key);

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  @override
  Widget build(BuildContext context) {
    return Center(

        child: GridView(
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
      ),
      primary: false,
      padding: const EdgeInsets.all(10),
      children: <Widget>[
        Container(
          padding: const EdgeInsets.all(10),
          margin: EdgeInsets.only(right: 10, bottom: 20),
          child: Image.asset("Images/apple logo.png"),
          color: Colors.orange,
        ),
        Container(
          padding: const EdgeInsets.all(8),
          margin: EdgeInsets.only(left: 10, bottom: 20),
          child: Image.asset("Images/welcome.png"),
          color: Colors.green[200],
        ),
        Container(
          padding: const EdgeInsets.all(8),
          margin: EdgeInsets.only(right: 10, bottom: 20),
          child: Image.asset("Images/facebook logo.png"),
          color: Colors.red[200],
        ),
        Container(
          padding: const EdgeInsets.all(8),
          margin: EdgeInsets.only(left: 10, bottom: 20),
          child: Image.asset("Images/Google logo.png"),
          color: Colors.purple[200],
        ),
        Container(
          padding: const EdgeInsets.all(8),
          margin: EdgeInsets.only(right: 10, bottom: 20),
          child: Image.asset("Images/1.png"),
          color: Colors.blueGrey[200],
        ),
        Container(
          padding: const EdgeInsets.all(8),
          margin: EdgeInsets.only(left: 10, bottom: 20),
          child: Image.asset("Images/whatsapp.png"),
          color: Colors.yellow[200],
          ),
      ],
    ),
    );
  }
}
