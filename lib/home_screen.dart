import 'package:flutter/material.dart';
import 'package:google_nav_bar/google_nav_bar.dart';

class MyHome extends StatefulWidget {

  @override
  _MyHome createState() {
    return new _MyHome();
  }
}

class _MyHome extends State<MyHome> {
  bool valuefirst = false;
  bool valuesecond = false;
  bool valuethird = false;
  String? gender;
  late final String title;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(title: Text("HomePage"),
          actions: [
            IconButton(
              icon: Icon(Icons.open_in_new),
              onPressed: showBottomSheet,
            ),
          ],
        ),

        drawer: Drawer(
          child: ListView(
            padding: const EdgeInsets.all(0),
            children: [
              const DrawerHeader(
                decoration: BoxDecoration(
                  color: Colors.blue,
                ), //BoxDecoration
                child: UserAccountsDrawerHeader(
                  decoration: BoxDecoration(color: Colors.blue),
                  margin: EdgeInsets.zero,
                  accountName: Text(
                    "Jay Gangani",
                    style: TextStyle(fontSize: 16),
                  ),
                  accountEmail: Text("jaygangani22@gmail.com"),
                  currentAccountPictureSize: Size.square(60),
                  currentAccountPicture: CircleAvatar(
                    backgroundImage: AssetImage("Images/welcome.png"),
                     //Text
                  ), //circleAvatar
                ), //UserAccountDrawerHeader
              ), //DrawerHeader
              ListTile(
                leading: const Icon(Icons.person),
                title: const Text(' My Profile '),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
              ListTile(
                leading: const Icon(Icons.settings),
                title: const Text(' My Settings '),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
              ListTile(
                leading: const Icon(Icons.safety_check),
                title: const Text(' About US '),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
              // ListTile(
              //   leading: const Icon(Icons.video_label),
              //   title: const Text(' Saved Videos '),
              //   onTap: () {
              //     Navigator.pop(context);
              //   },
              // ),
              // ListTile(
              //   leading: const Icon(Icons.edit),
              //   title: const Text(' Edit Profile '),
              //   onTap: () {
              //     Navigator.pop(context);
              //   },
              // ),
              ListTile(
                leading: const Icon(Icons.logout),
                title: const Text('LogOut'),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
            ],
          ),
        ),


        bottomNavigationBar: Container(
          width: 10,

          color: Colors.black,
          padding: const EdgeInsets.all(0),
          margin: EdgeInsets.only(right: 0, left: 0),

          child:
          GNav(
            backgroundColor: Colors.blue,
            color: Colors.white,
            activeColor: Colors.white,
            tabBackgroundColor: Colors.blue.shade800,
            gap: 0,
            onTabChange: (index){
              print(index);
            },
            padding: EdgeInsets.all(16),
            tabs: const [
              GButton(icon: Icons.home,
                text: 'Home',
              ),
              GButton(icon: Icons.favorite_border,
                text: 'Likes',
              ),
              GButton(icon: Icons.search,
                text: 'Search',
              ),
              GButton(icon: Icons.settings,
                text: 'Settings',
              ),
            ],
          ),
        ),//Drawer
        body: Container(
          child: Column(
            children: <Widget>[
              CheckboxListTile(
                controlAffinity: ListTileControlAffinity.trailing,
                title: const Text('Football'),
                value: this.valuefirst,
                onChanged: (value) {
                  setState(() {
                    this.valuefirst = value!;
                  });
                },
              ),
              CheckboxListTile(
                controlAffinity: ListTileControlAffinity.trailing,
                title: const Text('Cricket'),
                value: this.valuesecond,
                onChanged: (value) {
                  setState(() {
                    this.valuesecond = value!;
                  });
                },
              ),
              CheckboxListTile(
                controlAffinity: ListTileControlAffinity.trailing,
                title: const Text('basketball'),
                value: this.valuethird,
                onChanged: (value) {
                  setState(() {
                    this.valuethird = value!;
                  });
                },
              ),
              Container(

              ),
              Column(
                children: [

                  Text("What is your gender?", style: TextStyle(
                      fontSize: 18
                  ),),

                  Divider(),

                  RadioListTile(
                    title: Text("Male"),
                    value: "male",
                    groupValue: gender,
                    onChanged: (value){
                      setState(() {
                        gender = value.toString();
                      });
                    },
                  ),

                  RadioListTile(
                    title: Text("Female"),
                    value: "female",
                    groupValue: gender,
                    onChanged: (value){
                      setState(() {
                        gender = value.toString();
                      });
                    },
                  ),

                  RadioListTile(
                    title: Text("Other"),
                    value: "other",
                    groupValue: gender,
                    onChanged: (value){
                      setState(() {
                        gender = value.toString();
                      });
                    },
                  )
                ],
              ),

              Container(
                child: ElevatedButton(
                  onPressed: () {
                    Navigator.pushNamed(context, 'hobbies');
                  },
                  child: const Text('Next Page'),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void showBottomSheet() => showModalBottomSheet(
    enableDrag: false,
    isDismissible: false,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(24),
        topRight: Radius.circular(24),
      ),
    ),
    barrierColor: Colors.orange.withOpacity(0.2),
    context: context,
    builder: (context) => Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        ListTile(
          leading: Icon(Icons.share),
          title: Text('Share'),
          onTap: () {
            Navigator.of(context).pop(context);
          },
        ),
        ListTile(
          leading: Icon(Icons.link),
          title: Text('Copy link'),
          onTap: () => {},
        ),
      ],
    ),
  );
}