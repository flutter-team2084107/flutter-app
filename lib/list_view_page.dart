



import 'package:flutter/material.dart';




void main() => runApp(MyList());

class MyList extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text('ListView'),
        ),
        body: ListView(
          children: <Widget>[
            Container(
              height: 50,
              color: Colors.red[800],
              child: const Center(child: Text('Apple')),
            ),
            Container(
              height: 50,
              color: Colors.yellow[600],
              child: const Center(child: Text('Banana')),
            ),
            Container(
              height: 50,
              color: Colors.lime[400],
              child: const Center(child: Text('Mango')),
            ),
            Container(
              height: 50,
              color: Colors.orange[800],
              child: const Center(child: Text('Orange')),
            ),

            Container(
              padding: EdgeInsets.all(20),
              margin: EdgeInsets.all(20),
              child: ElevatedButton(
                
                onPressed: () {
                  Navigator.pushNamed(context, 'Listview');
                },
                child: const Text('Next Page'),
              ),
            ),

            

        ],
      ),
        ),

      );
    }


}
